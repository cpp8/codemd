include ./Makefile.std
EXEC=codemd
OBJLIB = ./lib/libcodemd.a 

all: $(OBJECTS) $(OBJLIB)
	$(CXX) $(OBJECTS) $(OBJLIB) $(LDFLAGS) -o $(EXEC)

$(OBJLIB): $(LIBOBJECTS)
	-mkdir ./lib 
	$(MAKE) -C options clean all
	$(MAKE) -C markdown clean all

clean:
	rm $(OBJLIB)
	rm -f $(EXEC)
	rm -f $(OBJECTS)

test:
	-./codemd
	-./codemd --help
	-./codemd -h
	./codemd --show-linenumbers markdown/markdown_test.txt