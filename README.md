# codemd code examples with markdown

## usage
```
 ./codemd --help

codemd [options] <file> ...:
  -v [ --verbose ]            set verbose
  -V [ --version ]            show version
  -h [ --help ]               produce help message
  -s [ --show-linenumbers ]   show line nunmbers
```
# markdown with line numbers

# Example markup in a source file

This is a fragment of the file markdown/markdown_test.txt

```

// codemd:begin caption="This is a caption"
Markdown::Markdown(std::string _filename) {
   cerr << "Markdown file " << _filename << endl ;

    ...

//codemd:skip

    string fullname = filesystem::absolute(_filename) ;
    if (Verbose) cout << "Full name " << fullname << endl ;

    ...
    
    ifile = ifstream(_filename, ios::binary) ;
    if (!ifile.is_open()) {
        cerr << "Error opening " << _filename << endl ;
        return ;
    }
//codemd:skipend
    fragments = 0 ;
    linenum = 0 ;
    string iline ;
    while (!ifile.eof()) {   
        getline(ifile,iline);
        linenum++ ;

    }
    ifile.close() ;
}
//codemd:end caption="Caption at the bottom of figure"
```
## correponding graphic 

![Graphic of the above file](markdown_test_3.png)