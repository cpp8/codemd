#include <iostream>

#include "options/options.hpp"
#include "markdown/markdown.hpp"
#include "markdown/graphic_markdown.hpp"
using namespace std ;

int main(int argc, char** argv) {
    Options options(argc, argv) ;
    options.Analyze() ;
    Markdown markdown;
    markdown.Verbose = options.Verbose ;
    markdown.Linenumbers = options.ShowLineNumbers ;

    for (auto arg=options.Arguments.begin(); arg != options.Arguments.end(); arg++) {
        if (options.Verbose) cout << "Processing " << *arg << endl ;
        GraphicMarkdown md (*arg) ;
    }
}