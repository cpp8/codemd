#include "fragment.hpp"

Fragment::Fragment(std::string _fragmentid) 
: id(_fragmentid) , caption("Listing") , bottom(true)
{

}

void Fragment::Add(std::string _line) {
    fraglines.push_back(_line);
}

void Fragment::SetCaption(std::string _caption, bool _bottom) {
    caption = _caption ;
    bottom = _bottom ;
}

std::string Fragment::Filename() const {
    return id + ".png" ;
}

int Fragment::Count() const {
    return fraglines.size();
}