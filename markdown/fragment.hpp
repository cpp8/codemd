#ifndef _FRAGMENT_HPP_
#define _FRAGMENT_HPP_
#include <string>
#include <vector>

typedef std::vector<std::string> fragment_lines;

class Fragment {
public:
    Fragment(std::string _fragmentid);
    void Add(std::string _line);
    void SetCaption(std::string _caption, bool _bottom);
    bool GetCaptionBottom() const { return bottom ; } ;
    std::string GetCaption() const { return caption ; } ;
    std::string Filename() const ;
    int Count() const ;
    fragment_lines& GetLines() { return fraglines ;} ;
private:
    Fragment();
    std::string id ;
    fragment_lines fraglines ;
    std::string caption ;
    bool bottom ;
} ;
#endif