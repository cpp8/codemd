#include <iostream>
#include <filesystem>
#include <boost/format.hpp>
#include "markdown.hpp"
#include "graphic_markdown.hpp"
#include "fragment.hpp"

using namespace std ;

const int LinePadding = 4 ;
const int CharHeight = 12 ;
const int LineHeight = CharHeight + LinePadding ;

const int ImageWidth = 600 ;
const int Margins = 20 ;
const int CaptionMult = 3 ;

const int CaptionHeight = 3 * LineHeight ;

std::string GraphicMarkdown::fontdir = "/usr/share/fonts/truetype";
std::string GraphicMarkdown::fontfile = "freefont/FreeSerif.ttf" ;
std::string GraphicMarkdown::fontfilename ;

GraphicMarkdown::GraphicMarkdown(string _filename) 
: fragimg(NULL) , fraglines(0) , fragment(NULL)
{
   if (Verbose) cout << "Graphic Markdown " << _filename << endl ;
   fontfilename = filesystem::path(fontdir) / fontfile ;
   if (Verbose) cout << "Font file name set to " << fontfilename << endl ;
   Process( _filename );
}

void GraphicMarkdown::startFragment(string _fragmentid) {
    if (Verbose) cout << "Starting fragment (Graphic) " << _fragmentid << endl ;
    fragment = new Fragment(_fragmentid) ;
    emitCaption("" , true) ;
}

void GraphicMarkdown::emitCaption(std::string _caption, bool _bottom) {
    std::string fragstr = to_string(fragments) ;
    std::string fullcaption = "Listing " + fragstr + " " + _caption ;
    fragment->SetCaption(fullcaption,_bottom) ;
}

void GraphicMarkdown::emitLine(std::string _line) {
    //string linenumstr = to_string(linenum) ;
    string linenumstr ;
    if (Linenumbers) {
        boost::format fmt("%4d") ;
        fmt % linenum ;
        linenumstr = fmt.str() + " : ";
    }
    fragment->Add( linenumstr + _line) ;
}

void GraphicMarkdown::emitEllipsis() {
    fragment->Add("") ;
    if (Linenumbers) {
        fragment->Add("       ...") ;  
    } else {
        fragment->Add("...") ;  
    }
    fragment->Add("") ;
}

void GraphicMarkdown::endFragment() {
    if (Verbose) cout << "End of fragment - Graphics" << endl ;
    if (Verbose) cout << "Fragment Image " << (unsigned long) fragimg << endl ;
    //fragimg->resize(imagewidth,fraglines * (linepadding*lineheight));
    int pngheight = fragment->Count() * LineHeight + CaptionHeight + LineHeight ;
    auto img = new pngwriter(ImageWidth, pngheight + LineHeight , 0 , fragment->Filename().c_str()) ;

    int y = pngheight ;
    if (fragment->GetCaptionBottom()) {
        img->plot_text( (char *)fontfilename.c_str()
                        , CharHeight
                        , Margins , LineHeight , 0.0 
                        , (char *)fragment->GetCaption().c_str() 
                        , 1.0 , 1.0 , 1.0 );
    } else {
        img->plot_text(  (char *)fontfilename.c_str()
                , CharHeight
                , Margins , y - LineHeight*(CaptionMult-1) , 0.0 
                , (char *)fragment->GetCaption().c_str() 
                , 1.0 , 1.0 , 1.0 );
    }

    if (!fragment->GetCaptionBottom()) {
       y = pngheight - CaptionHeight ;
    } else {
       y = pngheight ;
    }

    for (auto lineptr = fragment->GetLines().begin() ; lineptr != fragment->GetLines().end() ; lineptr++) {
        y -= LineHeight ;
        if (Verbose) cout << *lineptr << endl ;
        img->plot_text(  (char *)fontfilename.c_str()       
                        , CharHeight
                        , Margins , y , 0.0 
                        , (char *)lineptr->c_str() 
                        , 1.0 , 1.0 , 1.0 );
    }
    img->close();
}