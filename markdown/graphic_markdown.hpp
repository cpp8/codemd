#include <string>
#include <vector>
#include <pngwriter.h>
#include "markdown.hpp"
#include "fragment.hpp"

class GraphicMarkdown : public Markdown {
public:
    GraphicMarkdown(std::string _filename) ;
protected:
    virtual void startFragment(std::string _fragmentid) ;
    virtual void endFragment() ;
    virtual void emitCaption(std::string _captio, bool _bottom) ;
    virtual void emitLine(std::string _line) ;
    virtual void emitEllipsis() ;
    std::string fragmentid ;
    pngwriter *fragimg ;
    int fraglines;
    Fragment *fragment ;
    static std::string fontdir ;
    static std::string fontfile ;
    static std::string fontfilename ;
} ;
