#include <iostream>
#include <fstream>
#include <iomanip>
#include <filesystem>
#include <stdlib.h>
#include "markdown.hpp"

using namespace std ;

bool Markdown::Verbose = false ;
bool Markdown::Linenumbers = false ;
regex *Markdown::caption = NULL ;
Markdown::Markdown() {
    
}

Markdown::Markdown(std::string _filename) {
    Process(_filename) ;
}

void Markdown::Process(std::string _filename) {
    if (caption == NULL) {
        string captionexp = "caption=" ;
        captionexp.append( "\\" );
        captionexp.append( "\"(.*)\\\"");

        if (Verbose) cout << "Compiling regex " << captionexp << endl ;
        caption = new regex(captionexp) ;
    }
   cerr << "Markdown file " << _filename << endl ;
    if (!filesystem::is_regular_file(_filename)) {
        cerr << _filename << " is not a regular file" << endl ;
        return ;
    }
    string fullname = filesystem::absolute(_filename) ;
    if (Verbose) cout << "Full name " << fullname << endl ;

    filesystem::path p(fullname) ;
    basename = p.stem() ;
    folder = p.parent_path() ;
    if (Verbose) cout << "Base name " << basename << " in folder " << folder << endl ;
    
    ifile = ifstream(_filename, ios::binary) ;
    if (!ifile.is_open()) {
        cerr << "Error opening " << _filename << endl ;
        return ;
    }
    fragments = 0 ;
    linenum = 0 ;
    string iline ;
    while (!ifile.eof()) {   
        getline(ifile,iline);
        linenum++ ;
        if (iline.find( "codemd:begin") != string::npos ) {
            fragments++ ;
            emitFragment(iline) ;
        }
    }
    ifile.close() ;
}
void Markdown::findEmitCaption(string _line, bool _bottom) {
    if (Verbose) {
        cout << "Searching for caption" << endl ;
    }
    std::smatch caption_match;
    if (regex_search(_line, caption_match , *caption )) {
        if (Verbose) {
            for (size_t i = 0; i < caption_match.size(); ++i) 
               if (Verbose) cout << i << ": " << caption_match[i] << '\n';
        }
        emitCaption(caption_match[1],_bottom) ;
    }
}

void Markdown::emitFragment(std::string _beginline) {

    if (Verbose) cout << "Start fragment " << fragments << endl ;
    //string fragno = to_string(fragments) ;
    string fragid = filesystem::path(folder) / basename ;
    fragid += "_" + to_string(fragments) ;
    startFragment(fragid);
    findEmitCaption(_beginline,false);
    string iline ;
    bool skipping=false ;
    while (!ifile.eof()) {   
        getline(ifile,iline);
        linenum++ ;
        if (skipping) {
            if (iline.find( "codemd:skipend") != string::npos) {
                skipping = false ;
            }
            continue ;
        }

        if (iline.find( "codemd:end") != string::npos) {
            findEmitCaption(iline,true);
            endFragment() ;
            return ;
        }
 
        if (iline.find( "codemd:skip") != string::npos) {
            skipping = true ;
            emitEllipsis() ;
            continue ;
        }       
        emitLine(iline) ;
    }
}

void Markdown::emitLine(string _line) {
    if (Linenumbers) {
        cout << setw(3) << setfill('0') << linenum << " : " ;
    }
    if (Verbose) cout << _line << endl ;
}

void Markdown::emitCaption(string _caption, bool _bottom) {
    if (Verbose) cout << "Figure " << fragments << " : " << _caption << endl ;
}

void Markdown::emitEllipsis() {
    if (Verbose) {
        cout << endl ;
        cout << "       " ;
        cout << "..." << endl ;
        cout << endl ;
    }
}


void Markdown::startFragment(string _fragmentid) {
     cout << "Starting fragment " << _fragmentid << endl ;
}

void Markdown::endFragment() {
     cout << "End fragment " << endl ;
}