#ifndef MARKDOWN_HPP
#define MARKDOWN_HPP

#include <string>
#include <fstream>

#include <regex>

class Markdown {
public:
    Markdown();
    Markdown(std::string _filename) ;
    static bool Verbose ;
    static bool Linenumbers ;
protected:

    void Process(std::string _filename) ;
    void findEmitCaption(std::string _line,bool _bottom) ;
    void emitFragment(std::string _beginline) ;

    virtual void startFragment(std::string _fragmentid) ;
    virtual void endFragment() ;
    virtual void emitCaption(std::string _caption, bool _bottom) ;
    virtual void emitLine(std::string _line) ;
    virtual void emitEllipsis() ;
    std::string basename ;
    std::string folder ;
    std::ifstream ifile ;
    int fragments ;
    int linenum ;
    static std::regex *caption ;
} ;


#endif
