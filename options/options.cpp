#include <iostream>
#include <boost/program_options.hpp>
using namespace boost ;
namespace po = boost::program_options ;
#include <algorithm>
#include <iterator>
#include <string>
#include <vector>

#include "options.hpp"

using namespace std ;

template<class T>
ostream& operator<<(ostream& os, const vector<T>& v)
{
    copy(v.begin(), v.end(), ostream_iterator<T>(os, " "));
    return os;
}

Options::Options(int _argc, char **_argv) 
: argc(_argc) , argv(_argv) , Verbose(false) , ShowLineNumbers(false)
{

}

bool Options::Analyze() {


    po::options_description generic("codemd [options] <file> ...");
    generic.add_options()
        ("verbose,v", "set verbose")
        ("version,V" , "show version")
        ("help,h", "produce help message")    
        ("show-linenumbers,s" , "show line nunmbers")
        ;

    po::options_description hidden("Hidden options");
    hidden.add_options()
      ("input-file", po::value< vector<string> >(), "input file")
        ;

    po::options_description visible ;
    visible.add(generic)  ;

    po::options_description cmdline_options;
    cmdline_options.add(generic).add(hidden) ;   
    po::variables_map vm;

    po::positional_options_description pos ;
    pos.add("input-file",-1);

    if (argc < 2) {
        cerr << visible << endl ;
        return false ;
    }
    try {
        po::store(po::command_line_parser(argc, argv).options(cmdline_options).positional(pos).run(), vm);
    } 
    catch(std::exception& e) {
        cerr << e.what() << endl ;
        return false;
    } 
    po::notify(vm);    

    if (vm.count("help")) {
        cerr << visible << endl ;
        return false ;
    }

    if (vm.count("verbose")) {
        Verbose = true ;
    }

    if (vm.count("version")) {
        cerr << "codemd 00.01 - " << __DATE__ << " " << __TIME__ << endl ;
        return false ;
    }

    if (vm.count("show-linenumbers")) {
        ShowLineNumbers = true ;
    }

    if (vm.count("input-file"))
    {
        Arguments = vm["input-file"].as< vector<string> >() ;
        if (Arguments.size() < 1) {
            cerr << "Need some files to process" << endl ;
            return false ;
        }
    }

    if (Verbose) Show() ;
    return true ;
}

void Options::Show() const {
    cout << "ShowLineNumbers " << ShowLineNumbers << endl ;
    cout << "Input files are: " << Arguments << endl ;
}

