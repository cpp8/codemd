#ifndef _OPTIONS_HPP_
#define _OPTIONS_HPP_
#include <string>
#include <vector>

class Options {
public:
    Options(int _argc, char **_argv) ;
    bool Analyze() ;
    void Show() const ;
    bool Verbose ;
    bool ShowLineNumbers ;   
    std::vector< std::string > Arguments ;
private:
    int argc ;
    char **argv ;
    Options() ;
} ;

#endif