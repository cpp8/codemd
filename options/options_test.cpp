#include <iostream>
#include "options.hpp"
using namespace std ;
int main(int argc, char **argv) {
    Options options(argc, argv) ;
    if (options.Analyze()) {
        cout << "Continuing" << endl ;
    } else {
        exit(EXIT_FAILURE) ;
    }
}